$(document).ready(function() {  
    $("body").niceScroll({
        scrollspeed: 400,
        mousescrollstep: 40,
        cursorwidth: 5,
        cursorborder: 0,
        cursorcolor: '#2897AA',
        zindex: "10",
        cursorborderradius: 10,
        autohidemode: true,
        horizrailenabled: false,
        railpadding: { top: 0, right: 10, left: 0, bottom: 0 },
    });
});
/**
  *Smooth Scrool
  */
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        var st = $(this).scrollTop();
        if ( st >= about ) {
        $("hr").css("opacity", "1");
        $("hr").css("margin-left", "0");
        if ( st >= services ) {
            $("hr").css("margin-left", "25%");
            if ( st >= work ) {
                $("hr").css("margin-left", "50%");
                if ( st >= contact ) {
                    $("hr").css("margin-left", "75%");
                }
            }
        } 
        } else $("hr").css("opacity", "0");
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
// Hide Header on on scroll down
var ScrollBin = 0;
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();

// For Menu
var about = $("#about").offset().top;
var services = $("#services").offset().top;
var work = $("#work").offset().top;
var contact = $("#contact").offset().top;

$(window).scroll(function(event){
    var st = $(this).scrollTop();
    if ( ScrollBin == 0 ) {
    if ( st == 0 ) {
    $('header').addClass('fixed');
    }
    ScrollBin = 1;
    } else if ( ScrollBin == 1 ) {
        if ( st == 0 ) {
            $('header').removeClass('fixed');
        }
    }
    if ( st >= about ) {
        $("hr").css("opacity", "1");
        $("hr").css("margin-left", "0");
        if ( st >= services ) {
            $("hr").css("margin-left", "25%");
            if ( st >= work ) {
                $("hr").css("margin-left", "50%");
                if ( st >= contact ) {
                    $("hr").css("margin-left", "75%");
                }
            }
        } 
    } else $("hr").css("opacity", "0");
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('header').removeClass('fixed');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            if ( ScrollBin == 1 ) {
               if ( st == 0 ) {
                    $('header').removeClass('fixed');
                } else $('header').addClass('fixed');
            }
        }
    }
    
    lastScrollTop = st;
}
$( "#mobile-toggle-open-menu" ).click(function() {
  $( ".header-nav-holder-mobile" ).css('display',"table");
});
$( "#mobile-toggle-close-menu" ).click(function() {
  $( ".header-nav-holder-mobile" ).css('display',"none");
});